﻿#region Summary
/*
 * Asparuh Kamenov 
 * Created on 16/10/2016
 * 
 * Allows the user the pick the beats to be accented.
 * Sends the accentedBeats array to the main window, to be handled by the metronome class.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Metronome
{
    /// <summary>
    /// Interaction logic for SelectAccents.xaml
    /// </summary>
    public partial class SelectAccents : Window
    {
        private int maxBeats;

        public int[] AccentedBeats;

        public bool HasSetBeats = false;

        public SelectAccents(int maxBeats)
        {
            InitializeComponent();

            this.maxBeats = maxBeats;
            AccentedBeats = new int[maxBeats];

            for (int i = 1; i <= maxBeats; i++)
            {
                lBox_Beats.Items.Add(i);
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void winSelect_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (lBox_Beats.SelectedItems.Count != 0)
            {
                int[] getSelectedItems = new int[lBox_Beats.SelectedItems.Count];

                //Get the selected items numbers.
                for (int i = 0; i < lBox_Beats.SelectedItems.Count; i++)
                {
                    getSelectedItems[i] = (int)lBox_Beats.SelectedItems[i] - 1;
                }

                for (int i = 0; i < maxBeats; i++)
                {
                    for (int j = 0; j < lBox_Beats.SelectedItems.Count; j++)
                    {
                        if (i == getSelectedItems[j])
                        {
                            AccentedBeats[i] = 1;
                        }
                    }
                }

                HasSetBeats = true;
            }
        }
    }
}