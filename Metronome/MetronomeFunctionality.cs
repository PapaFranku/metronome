﻿#region Summary
/*
 * Asparuh Kamenov
 * 16/10/2016 - Added the functionality to chose accented beats.
 *            - Added the functionality to play only the accented beats.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Media;

namespace Metronome
{
    class MetronomeFunctionality
    {
        private Multimedia.Timer mmTimer;
        private SoundPlayer player;
        private SoundPlayer player2;

        private int counter = 1;
        private int beatsPerMeasure = 4;
        /// <summary>
        /// Gets or sets the number of beats per measure.
        /// </summary>
        public int BeatsPerMeasure
        {
            get { return beatsPerMeasure; }
            set 
            {
                foreach(int i in BeatsPerMeasureBoundaries)
                {
                    if(value == i)
                    {
                        beatsPerMeasure = value;
                        break;
                    }
                }
            }
        }

        private int measureLength = 4;
        /// <summary>
        /// Gets or sets the length of each beat.
        /// </summary>
        public int MeasureLength
        {
            get { return measureLength; }
            set 
            {
                foreach (int i in MeasureLengthBoundaries)
                {
                    if (value == i)
                    {
                        measureLength = value;
                        break;
                    }
                }
            }
        }

        private int bpm = 120;
        /// <summary>
        /// Gets or sets the BPM for the metronome.
        /// </summary>
        public int BPM
        {
            get { return bpm; }
            set 
            {
                if(value > 30 && value <= 600)
                {
                    bpm = value;
                }
            }
        }

        private int[] accentedBeats;
        /// <summary>
        /// Gets or sets the beats to be accented.
        /// A non-zero value means the beat at the given index should be accented.
        /// </summary>
        public int[] AccentedBeats
        {
            get { return accentedBeats; }
            set 
            {
                if(value.Length <= 17)
                {
                    accentedBeats = value;
                }
            }
        }

        private bool isRunning = false;
        /// <summary>
        /// Returns a value indicating whether the metronome is running.
        /// </summary>
        public bool IsRunning
        {
            get { return isRunning; }
        }

        private bool playAccentsOnly = false;
        /// <summary>
        /// Gets or sets the value indicating, whether only the accented beats should be played.
        /// </summary>
        public bool PlayAccentsOnly
        {
            get { return playAccentsOnly; }
            set { playAccentsOnly = value; }
        }

        /// <summary>
        /// A readonly collection containing the possible values for the beats per measure.
        /// </summary>
        public static readonly int[] BeatsPerMeasureBoundaries = { 2, 3, 4, 5, 6, 7,
                                                8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
        /// <summary>
        /// A readonly collection containing the possible values for the length of each beat.
        /// </summary>
        public static readonly int[] MeasureLengthBoundaries = { 4, 8, 16, 32 };

        public MetronomeFunctionality()
        {
            player = new SoundPlayer(Properties.Resources.HighBeat);
            player.SoundLocationChanged += new EventHandler(this.player_LocationChanged);
            player.Stream = Properties.Resources.HighBeat;
            player.Load();

            player2 = new SoundPlayer(Properties.Resources.LowBeat);
            player2.Stream = Properties.Resources.LowBeat;
            player2.Load();
            player2.SoundLocationChanged += new EventHandler(this.player2_LocationChanged);

            mmTimer = new Multimedia.Timer();
            mmTimer.Mode = Multimedia.TimerMode.Periodic;
            mmTimer.Tick += new EventHandler(this.mmTimer_Tick);
        }

        /// <summary>
        /// Starts the metronome.
        /// </summary>
        public void Start()
        {
            int setParams = bpmTOms();

            mmTimer.Period = setParams;
            mmTimer.Resolution = setParams * measureLength;

            if(accentedBeats == null || accentedBeats.Length != beatsPerMeasure)
            {
                accentedBeats = new int[beatsPerMeasure];
                accentedBeats[0] = 1;
                for(int i = 1; i < beatsPerMeasure; i++)
                {
                    accentedBeats[i] = 0;
                }
            }

            isRunning = true;
            mmTimer.Start();
        }

        /// <summary>
        /// Stops the metronome.
        /// </summary>
        public void Stop()
        {
            mmTimer.Stop();
            isRunning = false;
            counter = 1;
        }        

        private int bpmTOms()
        {
            int MS = 60000 / bpm;

            switch (MeasureLength)
            {
                case 4:
                    break;
                case 8:
                    MS = MS / 2;
                    break;
                case 16:
                    MS = MS / 4;
                    break;
                case 32:
                    MS = MS / 8;
                    break;
            }
            return MS;
        }

        private void mmTimer_Tick(object sender, EventArgs e)
        {
            if (counter > beatsPerMeasure)
            {
                counter = 1;
            }

            if (accentedBeats[counter - 1] != 0)
            {
                //player.Stop();
                player.Play();
            }
            else if (!playAccentsOnly)
            {
                //player2.Stop();
                player2.Play();
            }
            counter++;
        }

        private void player_LocationChanged(object sender, EventArgs e)
        {
            player.Load();
        }

        private void player2_LocationChanged(object sender, EventArgs e)
        {
            player2.Load();
        }

        /// <summary>
        /// The exception that is thrown when the MS is less than 100
        /// </summary>
        public class BpmOutOfRangeException : Exception
        {
            public BpmOutOfRangeException() : base()
            {

            }

            public BpmOutOfRangeException(string message) : base(message)
            {

            }
        }
    }
}