﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Metronome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MetronomeFunctionality Metro;
        private MetronomeFunctionality Metro2;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void chkboxIsPoly_Unchecked(object sender, RoutedEventArgs e)
        {
            cboxNoteNumber2.Visibility = Visibility.Hidden;
            cboxNoteLength2.Visibility = Visibility.Hidden;
            lblSlash2.Visibility = Visibility.Hidden;
        }

        private void chkboxIsPoly_Checked(object sender, RoutedEventArgs e)
        {
            cboxNoteNumber2.Visibility = Visibility.Visible;
            cboxNoteLength2.Visibility = Visibility.Visible;
            lblSlash2.Visibility = Visibility.Visible;
        }

        private void chkboxPlayAccentsOnly_Checked(object sender, RoutedEventArgs e)
        {
            Metro.PlayAccentsOnly = true;
        }

        private void chkboxPlayAccentsOnly_Unchecked(object sender, RoutedEventArgs e)
        {
            Metro.PlayAccentsOnly = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Metro.IsRunning == false)
            {
                Metro.BPM = int.Parse(txtBoxTempo.Text);               
                Metro.BeatsPerMeasure = (int)cboxNoteNumber.SelectedItem;
                Metro.MeasureLength = (int)cboxNoteLength.SelectedItem;

                cboxNoteLength.IsEnabled = false;
                cboxNoteNumber.IsEnabled = false;
                txtBoxTempo.IsEnabled = false;
                chkboxIsPoly.IsEnabled = false;
                chkboxPlayAccentsOnly.IsEnabled = false;
                btnAccents1.IsEnabled = false;

                if (chkboxIsPoly.IsChecked == true)
                {
                    Metro2.BPM = int.Parse(txtBoxTempo.Text);
                    Metro2.BeatsPerMeasure = (int)cboxNoteNumber.SelectedItem;
                    Metro2.MeasureLength = (int)cboxNoteLength.SelectedItem;

                    cboxNoteLength2.IsEnabled = false;
                    cboxNoteNumber2.IsEnabled = false;

                    Metro.Start();
                    Metro2.Start();
                }
                else
                {
                    Metro.Start();
                }
                btnRun.Content = "Stop";
            }
            else
            {
                cboxNoteLength.IsEnabled = true;
                cboxNoteNumber.IsEnabled = true;
                txtBoxTempo.IsEnabled = true;
                chkboxIsPoly.IsEnabled = true;
                chkboxPlayAccentsOnly.IsEnabled = true;
                btnAccents1.IsEnabled = true;

                if (chkboxIsPoly.IsChecked == true)
                {
                    cboxNoteLength2.IsEnabled = false;
                    cboxNoteNumber2.IsEnabled = false;

                    Metro.Stop();
                    Metro2.Stop();
                }
                else
                {
                    Metro.Stop();
                }
                btnRun.Content = "Start";
            }
        }

        private void btnAccents1_Click(object sender, RoutedEventArgs e)
        {
            SelectAccents windowAccents = new SelectAccents((int)cboxNoteNumber.SelectedItem);
            windowAccents.ShowDialog();

            if (windowAccents.HasSetBeats)
            {
                Metro.AccentedBeats = windowAccents.AccentedBeats;
            }
        }

        private void txtBoxTempo_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(txtBoxTempo.Text.Length > 3)
            {
                txtBoxTempo.Text = txtBoxTempo.Text.Substring(0, 3);
                txtBoxTempo.CaretIndex = 3;
            }
        }

        private void txtBoxTempo_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                int BPM = int.Parse(txtBoxTempo.Text);
                if (BPM < 30)
                {
                    txtBoxTempo.Text = "30";
                }
                else if (BPM > 600)
                {
                    txtBoxTempo.Text = "600";
                }
            }
            catch
            {
                txtBoxTempo.Text = "120";
            }
        }

        private void MainWindowApp_Loaded(object sender, RoutedEventArgs e)
        {
            Metro = new MetronomeFunctionality();
            Metro2 = new MetronomeFunctionality();

            cboxNoteNumber.ItemsSource = MetronomeFunctionality.BeatsPerMeasureBoundaries;
            cboxNoteNumber.SelectedIndex = 2;
            cboxNoteNumber2.ItemsSource = MetronomeFunctionality.BeatsPerMeasureBoundaries;
            cboxNoteNumber2.SelectedIndex = 2;
            cboxNoteLength.ItemsSource = MetronomeFunctionality.MeasureLengthBoundaries;
            cboxNoteLength.SelectedIndex = 0;
            cboxNoteLength2.ItemsSource = MetronomeFunctionality.MeasureLengthBoundaries;
            cboxNoteLength2.SelectedIndex = 0;         
        }
    }
}